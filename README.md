# Rennifew Project


## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Rennifew project

## Description
This is just simple example of repository


## Installation
`git clone https://gitlab.com/hugidonic/rennifew-project.git` - use this command to clone repository to your machine


## Authors
Author - Rennifew


## Project status
Active